﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;
using DG.Tweening;

public class PlayerController : MonoBehaviour {
    public GameManager gm;
    protected Animator anim;
    protected Rigidbody playerRigidbody;
    public Transform camdir;
    protected float charAngle;
    protected float direction;
    protected AnimatorStateInfo stateInfo;
    protected AnimatorTransitionInfo transInfo;
    public VariableJoystick variableJoystick;

    public float acceleration = 8f;
    float accelOrigin;
    public float maxVelocity = 8f;
    Vector3 newVelocity;
    public bool dead;
    public bool success;
    public GameObject bloodParticle;
    
    private bool isFloor;
    public AudioClip jumpSound;
    public AudioClip successSound;
    public AudioClip deadSound;

    protected void Awake()
    {
        accelOrigin = acceleration;
        anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();

    }
    private void Start()
    {
        gm = GameManager.instance;
    }
    // Update is called once per frame
    protected virtual void Update()
    {
        if (dead) return;
        stateInfo = anim.GetCurrentAnimatorStateInfo(0);
        transInfo = anim.GetAnimatorTransitionInfo(0);
        float xSpeed = acceleration * (variableJoystick.Horizontal + Input.GetAxis("Horizontal"));
        float zSpeed = acceleration * (variableJoystick.Vertical + Input.GetAxis("Vertical"));

        newVelocity = (xSpeed * camdir.right) + (zSpeed * camdir.forward);

        // newVelocity = new Vector3(xSpeed, 0, zSpeed);

        Vector2 moveSpeed = new Vector2(playerRigidbody.velocity.x, playerRigidbody.velocity.z);

        playerRigidbody.velocity = new Vector3(newVelocity.x, playerRigidbody.velocity.y, newVelocity.z);
    }
    public void Jump()
    {
        if (dead) return;
        if (isFloor)
        {
            playerRigidbody.AddForce(transform.up * 10, ForceMode.Impulse);
            gm.camAudio.PlayOneShot(jumpSound);
        }
    }
    protected void FixedUpdate()
    {
        if (dead) return;
        Ray rayDown = new Ray(new Vector3(transform.position.x, transform.position.y+0.1f, transform.position.z), -this.transform.up);
        RaycastHit rayDownhit;
        if (Physics.Raycast(rayDown, out rayDownhit, 0.2f) && rayDownhit.collider != gameObject)
            isFloor = true;
        else
            isFloor = false;
        anim.SetBool("isGounded", isFloor);
        AnimatorStateInfo info = anim.GetCurrentAnimatorStateInfo(0);
        float rootSpeed = 3;
        Vector3 lookrotation = new Vector3(newVelocity.x, 0, newVelocity.z);
        if (IsInPivot())
            rootSpeed = 10;
        if (lookrotation != Vector3.zero)
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(lookrotation), rootSpeed * Time.deltaTime);
        charAngle = 0f;
        direction = 0f;

        // Translate controls stick coordinates into world/cam/character space
        StickToWorldspace(this.transform, ref direction, ref charAngle, IsInPivot());
        anim.SetFloat("Speed", playerRigidbody.velocity.magnitude / 10 * 2, 0.05f, Time.deltaTime);
        anim.SetFloat("VerticalSpeed", -playerRigidbody.velocity.y, 0.02f, Time.deltaTime);
        anim.SetFloat("Direction", direction, 0.25f, Time.deltaTime);

        if (playerRigidbody.velocity.magnitude > LocomotionThreshold)    // Dead zone
            if (!IsInPivot())
                anim.SetFloat("Angle", charAngle);
        if (playerRigidbody.velocity.magnitude < LocomotionThreshold)    // Dead zone
        {
            anim.SetFloat("Direction", 0f);
            anim.SetFloat("Angle", 0f);
        }

    }
    public float LocomotionThreshold { get { return 0.2f; } }
    public bool IsInPivot()
    {
        return stateInfo.IsName("loco.LocomotionPivotL") ||
            stateInfo.IsName("loco.LocomotionPivotR") ||
            transInfo.IsName("loco.Locomotion -> loco.LocomotionPivotL") ||
            transInfo.IsName("loco.Locomotion -> loco.LocomotionPivotR");
    }
    public void StickToWorldspace(Transform root, ref float directionOut, ref float angleOut, bool isPivoting)
    {
        Vector3 rootDirection = root.forward;
        Vector3 stickDirection = new Vector3(newVelocity.x, 0, newVelocity.z);
        // Convert joystick input in Worldspace coordinates
        Vector3 moveDirection = stickDirection;
        Vector3 axisSign = Vector3.Cross(moveDirection, rootDirection);

        Debug.DrawRay(new Vector3(root.position.x, root.position.y + 2f, root.position.z), moveDirection, Color.green);
        Debug.DrawRay(new Vector3(root.position.x, root.position.y + 2f, root.position.z), rootDirection, Color.magenta);
        Debug.DrawRay(new Vector3(root.position.x, root.position.y + 2f, root.position.z), stickDirection, Color.blue);
        Debug.DrawRay(new Vector3(root.position.x, root.position.y + 2.5f, root.position.z), axisSign, Color.red);

        float angleRootToMove = Vector3.Angle(rootDirection, moveDirection) * (axisSign.y >= 0 ? -1f : 1f);
        if (!isPivoting)
        {
            angleOut = angleRootToMove;
        }
        angleRootToMove /= 180f;

        directionOut = angleRootToMove * 1.5f;
    }

    public void Dead()
    {
        gm.camAudio.PlayOneShot(deadSound);
        dead = true;
        anim.SetBool("Dead", true);
        bloodParticle.SetActive(true);
    }
    public void Success()
    {
        gm.camAudio.PlayOneShot(successSound);
        anim.CrossFadeInFixedTime("Success", 0);
        anim.SetBool("Success", true);
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().velocity *= 0;
        StartCoroutine(SuccessCoroutine());
    }
    IEnumerator SuccessCoroutine()
    {
        gm.MoveScene(SceneManager.GetActiveScene().buildIndex + 1, 5);
        while (true)
        {
            transform.LookAt(Camera.main.transform.position);
            Vector3 successPos = Camera.main.transform.position + Camera.main.transform.forward.normalized*3 - Camera.main.transform.up.normalized * 1;
            if(Vector3.Distance(transform.position, successPos) > 0.2f)
                transform.DOMove(successPos, 1);
            yield return new WaitForEndOfFrame();
        }
    }
}
