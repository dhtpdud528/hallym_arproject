﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public GameObject blockSpawnParticle;
    public Text stageIntro;
    public PlayerController player;
    public GameObject checkMarker;
    public GameObject[] dontdes;
    public AudioSource camAudio;
    public AudioClip blockSpawnSound;
    // Start is called before the first frame update
    private void Awake()
    {
        GameObject.Find("BackgroundMusic");
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 게임 매니저가 존재합니다!");
            Destroy(gameObject);
        }
        camAudio = Camera.main.GetComponent<AudioSource>();
    }
    void Start()
    {
        if(stageIntro!=null)
            stageIntro.text = "STAGE " + SceneManager.GetActiveScene().buildIndex;

        for(int i=0;i<dontdes.Length;i++)
            DontDestroyOnLoad(dontdes[i]);
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void MoveScene(int moveScene)
    {
        if (moveScene == 0)
            Destroy(GameObject.Find("BackgroundMusic"));
        SceneManager.LoadScene(moveScene);
        
    }
    public void MoveScene(int moveScene, float time)
    {
        StartCoroutine(MoveSceneCoroutine(moveScene, time));
    }
    IEnumerator MoveSceneCoroutine(int moveScene, float time)
    {
        yield return new WaitForSeconds(time);
        SceneManager.LoadScene(moveScene);
    }
    public void ResetScene()
    {
        Debug.Log(SceneManager.GetActiveScene());
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
