﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTrackableEventHandler : GameTrackableEventHandler
{
    protected override void OnTrackingFound()
    {
        base.OnTrackingFound();
        gm.player.gameObject.SetActive(true);
    }
}
