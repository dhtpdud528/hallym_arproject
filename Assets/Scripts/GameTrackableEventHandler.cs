﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class GameTrackableEventHandler : DefaultTrackableEventHandler
{
    public GameManager gm;
    protected override void Start()
    {
        base.Start();
        gm = GameManager.instance;
    }
    protected override void OnTrackingFound()
    {
        gm.camAudio.PlayOneShot(gm.blockSpawnSound);
        if (m_NewStatus == TrackableBehaviour.Status.TRACKED)
        {
            gm.checkMarker.SetActive(false);
            Instantiate(gm.blockSpawnParticle, transform.position + new Vector3(0, transform.localScale.y + 5, 0), Quaternion.identity);
        }
        base.OnTrackingFound();
    }
}
